This application generates a test paper of a given duration (for e.g. 90 or 120 minutes or any other duration paper) given the following information in JSON format :

1. Various question types and the number of questions that can be done in an hour as shown below:

"qnsPerHr": {
    "SINGLE_CHOICE": 20,
    "MULTI_CHOICE": 14,
    "PARAGRAPH": 7,
    "MATRIX": 14,
    "SUBJECTIVE": 14
  }

2. Various topics and their subtopic information including the # of questions in that sub-topic

 "topics": {
    "Calculus": {
      "Function": {
        "SINGLE_CHOICE": {
          "count": 98
        },
        "MULTI_CHOICE": {
          "count": 24,
        },
        "PARAGRAPH": {
            "count": 7,
        },
        "MATRIX": {
            "count": 7
        },
        "SUBJECTIVE": {
            "count": 38
        }
      }
 }


