Scenario 1 Given:

Per hr:
SC - 10
MC - 5
Paper - 90min
Questions:
SC - 20 
MC - 20 
**********

Total time
SC - 20 -> 2h
MC - 20 -> 4h

Weighted time
SC - .33
MC - .67

Split the exam time as per weighted time
SC -> .33 * 90 = 30 
MC -> .67 * 90 = 60

Build exams
Compute possible # of questions in each section as per the split
SC -> 10 * (30/60), MC -> 5 * (60/60)

Exam 1 -> SC: 5, MC: 5
Exam 2 -> SC: 5, MC: 5
Exam 3 -> SC: 5, MC: 5
Exam 4 -> SC: 5, MC: 5
******************************************************
Scenario 2 Given:

Per hr:
SC - 20
MC - 14
Paper - 90min
Questions:
SC - 98 
MC - 24 
**********

Total time
SC - 98 -> 294 min
MC - 24 -> 103 min

Weighted time
SC - .74
MC - .26

Split the exam time as per weighted time
SC -> .74 * 90 = 67 min 
MC -> .26 * 90 = 23 min

Compute possible # of questions in each section as per the split
SC -> 20 * (67/60), MC -> 14 * (23/60)

Build exams
Exam 1 -> SC: 23, MC: 6
Exam 2 -> SC: 23, MC: 6
Exam 3 -> SC: 23, MC: 6
Exam 4 -> SC: 23, MC: 6
Exam 5 -> SC: 6
******************************************************