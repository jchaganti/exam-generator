import ExamBuilder from './exam-builder';

const subject = 'maths';
const book = 'blackbook-VG-PJ';
const examDuration = 90;
const topics = ["Calculus.IDI"];
// const topics = ['Calculus.Function', 'Calculus.Limit'];

const examBuilder = new ExamBuilder(subject, book, topics, examDuration);
examBuilder.build();