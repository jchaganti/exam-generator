import { readFileSync, writeFileSync } from 'fs';
import stringify from 'json-stringify-pretty-compact';
import {
  difference, get, has, isEmpty,
  isNumber, isObject, range, shuffle,
  take, takeRight, union
} from 'lodash';
import path from 'path';


class ExamBuilder {
  private subject: string;
  private book: string;
  private topics: string[];
  private examDuration: number;
  private bookConfigData: any;

  constructor(subject: string, book: string, topics: string[], duration: number) {
    this.subject = subject;
    this.book = book;
    this.topics = topics;
    this.examDuration = duration;
    const filePath = path.join(
      __dirname,
      `../data/${subject}/${book}/data.json`,
    );
    const contents = readFileSync(filePath, { encoding: 'utf-8' });
    this.bookConfigData = JSON.parse(contents);
  }


  private populateWeightedTime = (totalTime: number) => {
    const {examDuration, topics, bookConfigData} = this;
    const {qnsPerHr} = bookConfigData;
    this.bookConfigData = topics.reduce((bookConfigData, topic) => {
      const topicData = get(bookConfigData, `topics.${topic}`);
      Object.keys(topicData).reduce((topicData, qType) => {
        const { shuffled, assigned } = topicData[qType];
        const unAssignedQs = difference(shuffled, assigned);
        const qTypeTime = (unAssignedQs.length * 60) / qnsPerHr[qType];
        const weightedTime = qTypeTime / totalTime;
        topicData[qType].weightedTime = Math.ceil(weightedTime * examDuration);
        return topicData;
      }, topicData);
      return bookConfigData;
    }, bookConfigData);
  };

  private getTotalTime = () => {
    const {topics, bookConfigData} = this;
    const {qnsPerHr} = bookConfigData;
    return topics.reduce((totalTime, topic) => {
      const topicData = get(bookConfigData, `topics.${topic}`);
      const topicTime = Object.keys(topicData).reduce((qTypeTime, qType) => {
        const { shuffled, assigned } = topicData[qType];
        const unAssignedQs = difference(shuffled, assigned);
        // console.log('unAssignedQs length', unAssignedQs.length);
        return unAssignedQs.length > 0
          ? qTypeTime + (unAssignedQs.length * 60) / qnsPerHr[qType]
          : qTypeTime;
      }, 0);
      return totalTime + topicTime;
    }, 0);
  };

  private shuffleQuestions = () => {
    const {topics, bookConfigData} = this;
    topics.forEach(topic => {
      const topicData = get(bookConfigData, `topics.${topic}`);
      Object.keys(topicData).forEach(key => {
        let { count, shuffled } = topicData[key];
        if (isEmpty(shuffled)) {
          shuffled = shuffle(range(1, count + 1));
          topicData[key].shuffled = shuffled;
        }
      });
    });
  };

  private getAssignedExamDuration = (exam: any) => {
    const {qnsPerHr} = this.bookConfigData;
    const duration = Object.values(exam).reduce((totalDuration: number, examQs: any) => {
      if (isObject(examQs)) {
        const topicDuration: number = Object.keys(examQs).reduce(
          (topicTime: number, qType: string) => {
            const numQues = (examQs as any)[qType].length;
            const duration = (numQues * 60) / qnsPerHr[qType];
            return topicTime + duration;
          },
          0,
        );
        return totalDuration + topicDuration;
      } else {
        return totalDuration;
      }
    }, 0);
    return Math.ceil(duration);
  };

  private generateExam = () => {
    const {topics, examDuration, bookConfigData} = this;
    const {qnsPerHr} = bookConfigData
    return topics.reduce((exam: any, topic: string) => {
      const topicData = get(bookConfigData, `topics.${topic}`);
      const topicExamQs = Object.keys(topicData).reduce(
        (topicExamQs: any, qType: string) => {
          const { shuffled, weightedTime, assigned = [] } = topicData[qType];
          const unAssignedQs = difference(shuffled, assigned);
          if (!isEmpty(unAssignedQs) && exam.duration < examDuration) {
            const qnsPerHrOfType = qnsPerHr[qType];
            let numOfQs = Math.ceil(qnsPerHrOfType * (weightedTime / 60));
            const numOfQsDuration = (numOfQs * 60) / qnsPerHrOfType;
            let additionalTime = numOfQsDuration;
            // console.log('numOfQsDuration', numOfQsDuration, 'weightedTime', weightedTime, ' OLD exam.duration', exam.duration)

            if (Math.ceil(exam.duration + numOfQsDuration) > examDuration) {
              const deltaTime = exam.duration + numOfQsDuration - examDuration;
              // console.log('DELTA_TIME', deltaTime, 'oldnumOfQs', numOfQs )
              const deltaQs = Math.ceil(qnsPerHrOfType * (deltaTime / 60));
              numOfQs = numOfQs - deltaQs;
              additionalTime = Math.ceil((numOfQs * 60) / qnsPerHrOfType);
            }
            exam.duration = exam.duration + additionalTime;
            // console.log('NEW exam.duration', exam.duration, 'numOfQs', numOfQs )

            const qNos = take(unAssignedQs, numOfQs).sort((a, b) => (a as unknown as number) - (b as unknown as number));
            topicData[qType].assigned = assigned.concat(qNos).sort((a: number, b: number) => a - b);
            topicExamQs[qType] = qNos;
          }
          return topicExamQs;
        },
        {},
      );
      exam[topic] = topicExamQs;
      return exam;
    }, { duration: 0 });
  };

  private buildExam = () => {
    const {getTotalTime, populateWeightedTime, generateExam} = this;
    // Compute total time for the unassigned questions
    const totalTime = getTotalTime();
    populateWeightedTime(totalTime );
    return generateExam();
  };

  private mergeTopic = (mergedExamTopic: any, qType: string, currentExam: any, key: string) => {
    const mergeTopicQs = mergedExamTopic[qType] || [];
    const currentExamTopicQs = currentExam[key][qType] || [];
    mergedExamTopic[qType] = mergeTopicQs.concat(currentExamTopicQs).sort((a: number, b: number) => a - b);
    return mergedExamTopic;
  }

  private mergeTopics = (mergedExam: any, key: string, currentExam: any) => {
    const {mergeTopic} = this;
    const mergedExamKeys = Object.keys(mergedExam[key]);
    const examKeys = Object.keys(currentExam[key]);
    const allKeys = union(mergedExamKeys, examKeys);
    mergedExam[key] = allKeys.reduce((mergedExamTopic, qType) => {
      return mergeTopic(mergedExamTopic, qType, currentExam, key);
    }, mergedExam[key]);
  }

  private mergeExamsFromIndex = (exams: any[], index: any) => {
    const {mergeTopics} = this;
    const examsTobeMerged = takeRight(exams, exams.length - 1 - index);
    const mergedExam = examsTobeMerged.reduce((mergedExam, currentExam) => {
      Object.keys(mergedExam).forEach(key => {
        if (isNumber(mergedExam[key])) {
          mergedExam[key] = mergedExam[key] + currentExam[key];
        } else if (isObject(mergedExam[key])) {
          mergeTopics(mergedExam, key, currentExam);
        }
      });
      return mergedExam;
    }, exams[index]);
    const otherExams = exams.filter((exam, i) => i < index);
    return otherExams.concat(mergedExam);
  }

  private getMergeFromIndex = (exams: any[], toleratedDuration: number): { index: any; _: any; } => {
    return exams.reduceRight((acc, exam, index) => {
      const { totalDuration } = acc;
      if (totalDuration + exam.duration < toleratedDuration) {
        return { index, totalDuration: totalDuration + exam.duration };
      } else {
        return acc;
      }
    }, { index: 0, totalDuration: 0 });
  }


  private mergeExams = (exams: any[], tolerance: number) => {
    const {examDuration, getMergeFromIndex, mergeExamsFromIndex} = this;
    const toleratedDuration = examDuration + tolerance;
    const { index, _ } = getMergeFromIndex(exams, toleratedDuration);
    return index < exams.length - 1 ? mergeExamsFromIndex(exams, index) : exams;
  }

  private buildExams = (count: number) => {
    const {bookConfigData, buildExam, getAssignedExamDuration} = this;
    let exam = buildExam();
    // Compute the exam duration based on the questions assigned
    exam.duration = getAssignedExamDuration(exam);
    if (exam.duration > 0) {
      const { exams = [] } = bookConfigData;
      const {book, topics} = this;
      exam.title = `${book}-${topics.join('-')}-exam-${++count}`;
      exams.push(exam);
      bookConfigData.exams = exams;
    }
    return { computedDuration: exam.duration, count };
  }

  private writeToFile = () => {
    const {bookConfigData, subject, book} = this;
    const filePath = path.join(__dirname, `../data/${subject}/${book}/data.json`);
    const bookJsonContents = stringify(bookConfigData, { indent: 2, maxLength: 100 });
    writeFileSync(filePath, bookJsonContents, { encoding: "utf-8" });
  }

  public build = () => {
    const {bookConfigData, topics, writeToFile, shuffleQuestions, buildExams, mergeExams} = this;
    try {
      const allValidTopics = topics.every(topic => has(bookConfigData, `topics.${topic}`));
      if (allValidTopics) {
        shuffleQuestions();
        let computedDuration = 0, count = 0;
        do {
          ({ computedDuration, count } = buildExams(count));
        } while (computedDuration > 0)
        bookConfigData.exams = mergeExams(bookConfigData.exams, 5);
        if (count > 0) {
          writeToFile();
        }
      }
    } catch (e) {
      console.log(e);
    }
  }
}

export default ExamBuilder;